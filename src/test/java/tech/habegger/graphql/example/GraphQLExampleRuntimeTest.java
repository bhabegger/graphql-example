package tech.habegger.graphql.example;


import graphql.ExecutionResult;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class GraphQLExampleRuntimeTest {

    GraphQLExampleRuntime runtime;

    GraphQLExampleRuntimeTest() throws IOException {
        runtime = new GraphQLExampleRuntime();
    }

    @Test
    public void queryWithFiltering() {
        // Given
        String query = """
            {
                countries(isoCodes: ["CH"]) {
                    isoCode
                    name
                    population
                    area
                    capital
                    currency
                }
            }
        """;

        // When
        ExecutionResult result = runtime.execute(query, Map.of());

        // Then
        assertThat(result.getErrors()).isEmpty();
        Map<String, Object> data = result.getData();
        assertThat(data).isNotNull();
        assertThat(data.get("countries")).isInstanceOf(List.class);
        List<Map<String, Object>> countries = (List<Map<String, Object>>) data.get("countries");
        assertThat(countries).hasSize(1);
        Map<String, Object> switzerland = countries.get(0);
        assertThat(switzerland).extracting("isoCode").isEqualTo("CH");
        assertThat(switzerland).extracting("name").isEqualTo("Switzerland");
        assertThat(switzerland).extracting("population").isEqualTo("8,563,760 (2023 est.)");
        assertThat(switzerland).extracting("area").isEqualTo("41,277 sq km");
        assertThat(switzerland).extracting("capital").isEqualTo("Bern");
        assertThat(switzerland).extracting("currency").isEqualTo("CHF");
    }
}