package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.
// Code generated by github.com/99designs/gqlgen version v0.17.44

import (
	"context"

	"gitlab.com/bhabegger/graphql-example/data"
	"gitlab.com/bhabegger/graphql-example/models"
)

// Countries is the resolver for the countries field.
func (r *queryResolver) Countries(ctx context.Context, isoCodes []*string) ([]*models.Country, error) {
	var countries []*models.Country

	for i := 0; i < len(data.IsoCodes); i++ {
		var code = data.IsoCodes[i].GetIsoCode()
		if Contains(isoCodes, code) {
			country := models.Country{
				IsoCode: data.IsoCodes[i].GetIsoCode(),
			}
			countries = append(countries, &country)
		}
	}
	return countries, nil
}

// Query returns QueryResolver implementation.
func (r *Resolver) Query() QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver }

// !!! WARNING !!!
// The code below was going to be deleted when updating resolvers. It has been copied here so you have
// one last chance to move it out of harms way if you want. There are two reasons this happens:
//   - When renaming or deleting a resolver the old code will be put in here. You can safely delete
//     it when you're done.
//   - You have helper methods in this file. Move them out to keep these resolver files clean.
func PointerTo[T any](v T) *T {
	return &v
}
func Contains(sl []*string, wanted string) bool {
	for _, v := range sl {
		if *v == wanted {
			return true
		}
	}
	return false
}
