package models

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/yalp/jsonpath"
	"gitlab.com/bhabegger/graphql-example/data"
)

const baseDbpediaQuery = "select * where {\n" +
	"   ?country a dbo:Country.\n" +
	"   dbr:ISO_3166-1:%s dbo:wikiPageRedirects ?country.\n" +
	"   ?country dbo:currency ?currency.\n" +
	"   ?currency dbp:isoCode ?currencyCode.\n" +
	"} LIMIT 1"

const baseDbpediaUrl = "https://dbpedia.org/sparql?" +
	"default-graph-uri=http%%3A%%2F%%2Fdbpedia.org&" +
	"query=%s&" +
	"format=application%%2Fsparql-results%%2Bjson&" +
	"timeout=30000&" +
	"signal_void=on&" +
	"signal_unconnected=on"

const baseWsbUrl = "https://raw.githubusercontent.com/factbook/factbook.json/master/%s/%s.json"

type Country struct {
	IsoCode    string
	WsfCountry *interface{}
}

func (c *Country) Currency() string {
	var sparql = fmt.Sprintf(baseDbpediaQuery, c.IsoCode)
	var url = fmt.Sprintf(baseDbpediaUrl, url.QueryEscape(sparql))
	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("Error %s", err)
		return ""
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)

	var result interface{}
	json.Unmarshal(body, &result)

	currency, _ := jsonpath.Read(result, "$.results.bindings[0].currencyCode.value")
	return fmt.Sprintf("%v", currency)
}

func (c *Country) Capital() string {
	return c.wsfPath("$.Government.Capital.name.text")
}

func (c *Country) Area() string {
	return c.wsfPath("$.Geography.Area.total.text")
}

func (c *Country) Population() string {
	return c.wsfPath("$.People and Society.Population.text")
}

func (c *Country) Name() string {
	return c.wsfPath("$.Government.Country name.conventional short form.text")
}

func (c *Country) wsfPath(path string) string {
	c.loadWsfCountry()
	value, _ := jsonpath.Read(c.WsfCountry, path)
	return fmt.Sprintf("%v", value)
}

func (c *Country) loadWsfCountry() {
	if c.WsfCountry == nil {
		var isoCode = data.LookupIsoCode(c.IsoCode)
		var url = fmt.Sprintf(baseWsbUrl, isoCode.GetContinent(), isoCode.GetWfbCode())
		resp, err := http.Get(url)
		if err != nil {
			fmt.Printf("Error %s", err)
			return
		}
		body, _ := io.ReadAll(resp.Body)
		var result interface{}
		json.Unmarshal(body, &result)
		c.WsfCountry = &result
	}
}
