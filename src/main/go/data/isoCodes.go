package data

type IsoCode struct {
	isoCode      string
	wfbContinent string
	wfbCode      string
}

var CH = IsoCode{isoCode: "CH", wfbContinent: "europe", wfbCode: "sz"}
var FR = IsoCode{isoCode: "FR", wfbContinent: "europe", wfbCode: "fr"}
var US = IsoCode{isoCode: "US", wfbContinent: "north-america", wfbCode: "us"}

var isoCodes = map[string]IsoCode{
	"CH": CH,
	"FR": FR,
	"US": US,
}

func (c *IsoCode) GetIsoCode() string {
	return c.isoCode
}

func (c *IsoCode) GetContinent() string {
	return c.wfbContinent
}

func (c *IsoCode) GetWfbCode() string {
	return c.wfbCode
}

var IsoCodes = valuesOf(isoCodes)

func valuesOf(c map[string]IsoCode) []IsoCode {
	var values []IsoCode
	for _, value := range c {
		values = append(values, value)
	}
	return values
}

func LookupIsoCode(code string) IsoCode {
	return isoCodes[code]
}
