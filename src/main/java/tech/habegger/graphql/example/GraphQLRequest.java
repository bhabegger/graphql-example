package tech.habegger.graphql.example;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public record GraphQLRequest(
        String query,
        Map<String, Object> variables,
        String operationName)
{ }
