package tech.habegger.graphql.example.model;

public record Country(String isoCode, String name) {
}
