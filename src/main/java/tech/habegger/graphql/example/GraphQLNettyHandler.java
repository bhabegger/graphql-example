package tech.habegger.graphql.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import graphql.ExecutionResult;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;

public class GraphQLNettyHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
    private Logger LOG = LoggerFactory.getLogger(GraphQLNettyHandler.class);
    private final static ObjectMapper MAPPER = new ObjectMapper();
    private final GraphQLExampleRuntime graphQLRuntime;

    public GraphQLNettyHandler() throws IOException {
        graphQLRuntime = new GraphQLExampleRuntime();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg) throws IOException {
        HttpMethod method = msg.method();
        String uri = msg.uri();
        CharSequence mimeType = HttpUtil.getMimeType(msg);
        Charset charset = HttpUtil.getCharset(msg, StandardCharsets.UTF_8);
        String query;
        Map<String, Object> variables;
        if("application/graphql".contentEquals(mimeType)) {
            query = msg.content().toString(charset);
            variables = Map.of();
        } else if("application/json".contentEquals(mimeType)) {
            GraphQLRequest request = unmarshalRequest(msg.content());
            query = request.query();
            variables = request.variables();
        } else {
            throw new UnsupportedOperationException("Charset %s is not supported".formatted(charset));
        }
        LOG.info("{} {}, query={} variables={}", method, uri, query.trim().replaceAll("\\s+"," "), MAPPER.writeValueAsString(variables));
        ExecutionResult result = graphQLRuntime.execute(query, variables);
        ByteBuf content = Unpooled.copiedBuffer(MAPPER.writeValueAsString(result), StandardCharsets.UTF_8);
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, content);
        response.headers().set(CONTENT_TYPE, "application/json; charset=UTF-8");
        response.headers().set(HttpHeaderNames.CONTENT_LENGTH, content.readableBytes());
        ctx.write(response);
        ctx.flush();
    }

    private GraphQLRequest unmarshalRequest(ByteBuf content) throws IOException {
        return MAPPER.readValue(new InputStreamReader(new ByteBufInputStream(content), StandardCharsets.UTF_8), GraphQLRequest.class);
    }
}
