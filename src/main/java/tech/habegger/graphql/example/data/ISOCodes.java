package tech.habegger.graphql.example.data;

public enum ISOCodes {
    CH("europe", "sz"),
    FR("europe", "fr"),
    ES("europe", "sp"),
    US("north-america", "us");

    private final String wfbContinent;
    private final String wfbCode;

    ISOCodes(String wfbContinent, String wfbCode) {
        this.wfbContinent = wfbContinent;
        this.wfbCode = wfbCode;
    }

    public String getWfbContinent() {
        return wfbContinent;
    }

    public String getWfbCode() {
        return wfbCode;
    }
}
