package tech.habegger.graphql.example.data;

import tech.habegger.graphql.example.model.Country;

import java.util.List;

public class StaticData {
    public static List<Country> COUNTRIES = List.of(
        new Country("CH", "Switzerland"),
        new Country("US", "United States Of America"),
        new Country("FR", "France"),
        new Country("ES", "Spain")
    );
}
