package tech.habegger.graphql.example;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.DelegatingDataFetchingEnvironment;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import tech.habegger.graphql.example.fetching.DBPediaCountryCurrencyDataFetcher;
import tech.habegger.graphql.example.fetching.WFBCountryDataFetcher;
import tech.habegger.graphql.example.fetching.WFBFieldDataFetcher;
import tech.habegger.graphql.example.mutation.FavoriteCountryDataFetcher;
import tech.habegger.graphql.example.mutation.Memory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class GraphQLExampleRuntime {

    GraphQL graphQL;

    public GraphQLExampleRuntime() throws IOException {
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        TypeDefinitionRegistry typeRegistry = buildRegistry();
        RuntimeWiring runtimeWiring = buildWiring();
        GraphQLSchema schema = schemaGenerator.makeExecutableSchema(typeRegistry, runtimeWiring);
        graphQL = GraphQL.newGraphQL(schema).build();
    }
    public ExecutionResult execute(String query, Map<String, Object> variables) {
        ExecutionInput.Builder executionInput = ExecutionInput.newExecutionInput()
            .query(query);
        if(variables != null) {
            executionInput.variables(variables);
        }
        return graphQL.execute(executionInput.build());
    }

    static TypeDefinitionRegistry buildRegistry() throws IOException {

        SchemaParser schemaParser = new SchemaParser();
        try(InputStream is = GraphQLExampleRuntime.class.getResourceAsStream("/schema.graphqls")) {
            assert is != null;
            Reader reader =  new InputStreamReader(is, StandardCharsets.UTF_8);
            return schemaParser.parse(reader);
        }
    }

    static RuntimeWiring buildWiring() {
        SimpleHttpClient httpClient = new SimpleHttpClient();
        Memory<String> favoriteCountryMemory = new Memory<>();

        WFBCountryDataFetcher countryDataFetcher = new WFBCountryDataFetcher(httpClient);
        RuntimeWiring.Builder runtimeWiring = RuntimeWiring.newRuntimeWiring()
            .type("Query", builder -> builder
                .dataFetcher("countries", (env) -> {
                    List<String> isoCodes = env.getArgument("isoCodes");
                    return isoCodes
                        .stream()
                        .map(isoCode -> adjustSource(env, isoCode))
                        .map(localEnv -> countryDataFetcher.get(localEnv))
                        .toList();
                })
                .dataFetcher("favoriteCountry", (env) -> countryDataFetcher.get(adjustSource(env, favoriteCountryMemory.get())))
            )
            .type("Mutation", builder -> builder
                .dataFetcher("favoriteCountry", new FavoriteCountryDataFetcher(favoriteCountryMemory))
            )
            .type("Country", builder -> builder
                .dataFetcher("area", new WFBFieldDataFetcher("/Geography/Area/total/text"))
                .dataFetcher("population", new WFBFieldDataFetcher("/People and Society/Population/text"))
                .dataFetcher("name", new WFBFieldDataFetcher("/Government/Country name/conventional short form/text"))
                .dataFetcher("capital", new WFBFieldDataFetcher("/Government/Capital/name/text"))
                .dataFetcher("currency", new DBPediaCountryCurrencyDataFetcher(httpClient))
            );
        return runtimeWiring.build();
    }

    private static DataFetchingEnvironment adjustSource(DataFetchingEnvironment original, String x) {
        return new DelegatingDataFetchingEnvironment(original) {
            @Override
            public <T> T getSource() {
                return (T)x;
            }
        };
    }
}