package tech.habegger.graphql.example;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicBoolean;

public class NettyGraphQL {
    final AtomicBoolean running = new AtomicBoolean(false);
    private static final int HTTP_PORT = 8080;

    private static Logger LOG = LoggerFactory.getLogger(NettyGraphQL.class);
    private ChannelFuture httpChannel;

    public void run() throws Exception {
        LOG.info("Starting GraphQL Netty Server");
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap httpBootstrap = new ServerBootstrap();
            // Configure the server
            httpBootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new GraphQLNettyInitializer()) // <-- Our handler created here
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            // Bind and start to accept incoming connections.
            httpChannel = httpBootstrap.bind(HTTP_PORT).sync();

            running.set(true);
            // Wait until server socket is closed
            httpChannel.channel().closeFuture().sync();
        } finally {
            LOG.info("Shutting down GraphQL Netty Server");
            synchronized (running) {
                workerGroup.shutdownGracefully();
                bossGroup.shutdownGracefully();
                running.set(false);
                running.notifyAll();
            }
        }
    }

    private void stop() {
        if(httpChannel != null) {
            httpChannel.channel().close();
        }
        while(running.get()) {
            try {
                synchronized (running) {
                    running.wait();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println("Bye");
    }
    public static void main(String[] args) throws Exception {
        NettyGraphQL server = new NettyGraphQL();
        Thread shutdown = new Thread(server::stop);
        Runtime.getRuntime().addShutdownHook(shutdown);
        server.run();
    }

}
