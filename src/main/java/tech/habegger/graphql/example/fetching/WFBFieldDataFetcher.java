package tech.habegger.graphql.example.fetching;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

public class WFBFieldDataFetcher implements DataFetcher<String> {
    private final String jsonPath;

    public WFBFieldDataFetcher(String jsonPath) {
        this.jsonPath = jsonPath;
    }

    @Override
    public String get(DataFetchingEnvironment environment) {
        CountryAccess countryAccess = environment.getSource();
        return countryAccess.wfbData().get().at(jsonPath).asText();
    }
}
