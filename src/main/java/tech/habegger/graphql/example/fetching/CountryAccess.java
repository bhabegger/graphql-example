package tech.habegger.graphql.example.fetching;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.function.Supplier;

public record CountryAccess(String isoCode, Supplier<JsonNode> wfbData) {
}
