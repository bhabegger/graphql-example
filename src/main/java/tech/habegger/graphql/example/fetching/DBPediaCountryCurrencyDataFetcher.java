package tech.habegger.graphql.example.fetching;

import com.fasterxml.jackson.databind.JsonNode;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import tech.habegger.graphql.example.SimpleHttpClient;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class DBPediaCountryCurrencyDataFetcher implements DataFetcher<String> {
    private static final String DBPEDIA_QUERY = "https://dbpedia.org/sparql?" +
            "default-graph-uri=http%%3A%%2F%%2Fdbpedia.org&" +
            "query=%s&" +
            "format=application%%2Fsparql-results%%2Bjson&" +
            "timeout=30000&" +
            "signal_void=on&" +
            "signal_unconnected=on";
    private static final String CURRENCY_QUERY = """
        select * where {
           ?country a dbo:Country.
           dbr:ISO_3166-1:%s dbo:wikiPageRedirects ?country.
           ?country dbo:currency ?currency.
           ?currency dbp:isoCode ?currencyCode.
        } LIMIT 1
        """;
    private final SimpleHttpClient httpClient;

    public DBPediaCountryCurrencyDataFetcher(SimpleHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public String get(DataFetchingEnvironment environment) throws Exception {
        CountryAccess countryAccess = environment.getSource();
        return fetchCountryCurrency(countryAccess.isoCode());
    }

    private String fetchCountryCurrency(String isoCode) throws IOException, InterruptedException {
        URI location = dbpediaCurrencyLookupLocation(isoCode);
        JsonNode result = httpClient.fetch(location);
        return result.at("/results/bindings/0/currencyCode/value").asText();
    }

    private URI dbpediaCurrencyLookupLocation(String isoCode) {
        String sparqlQuery = CURRENCY_QUERY.formatted(isoCode);
        String encodedQuery = URLEncoder.encode(sparqlQuery, StandardCharsets.UTF_8);
        try {
            return new URI(DBPEDIA_QUERY.formatted(encodedQuery));
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
