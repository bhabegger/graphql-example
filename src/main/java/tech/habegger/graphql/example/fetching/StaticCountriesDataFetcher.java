package tech.habegger.graphql.example.fetching;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import tech.habegger.graphql.example.model.Country;

import java.util.List;

import static tech.habegger.graphql.example.data.StaticData.COUNTRIES;

public class StaticCountriesDataFetcher implements DataFetcher<List<Country>> {
    @Override
    public List<Country> get(DataFetchingEnvironment environment) {
        List<String> wantedIsoCodes = environment.getArgument("isoCodes");
        if(wantedIsoCodes == null) {
            return COUNTRIES;
        } else {
            return COUNTRIES.stream().filter(country -> wantedIsoCodes.contains(country.isoCode())).toList();
        }
    }
}
