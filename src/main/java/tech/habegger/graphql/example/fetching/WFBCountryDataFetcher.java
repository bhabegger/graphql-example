package tech.habegger.graphql.example.fetching;

import com.fasterxml.jackson.databind.JsonNode;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import tech.habegger.graphql.example.SimpleHttpClient;
import tech.habegger.graphql.example.data.ISOCodes;

import java.net.URI;
import java.net.URISyntaxException;

public class WFBCountryDataFetcher implements DataFetcher<CountryAccess> {
    private final static String WFB_URI_PATTERN = "https://raw.githubusercontent.com/factbook/factbook.json/master/%s/%s.json";
    private final SimpleHttpClient httpClient;

    public WFBCountryDataFetcher(SimpleHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public CountryAccess get(DataFetchingEnvironment environment) {
        String isoCode = environment.getSource();
        return new CountryAccess(isoCode, () -> fetchWfbCountry(isoCode));
    }

    private JsonNode fetchWfbCountry(String isoCode) {
        URI location = wfbLocationOf(ISOCodes.valueOf(isoCode));
        return httpClient.fetchUnchecked(location);
    }

    private URI wfbLocationOf(ISOCodes isoCodes) {
        try {
            return new URI(WFB_URI_PATTERN.formatted(isoCodes.getWfbContinent(), isoCodes.getWfbCode()));
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

}
