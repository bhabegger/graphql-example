package tech.habegger.graphql.example.mutation;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

public class FavoriteCountryDataFetcher implements DataFetcher<String> {

    @Override
    public String get(DataFetchingEnvironment environment) throws Exception {
        String newValue =  environment.getArgument("isoCode");
        favoriteCountryMemory.set(newValue);
        return newValue;
    }

    public FavoriteCountryDataFetcher(Memory<String> favoriteCountryMemory) {
        this.favoriteCountryMemory = favoriteCountryMemory;
    }

    private final Memory<String> favoriteCountryMemory;
}
