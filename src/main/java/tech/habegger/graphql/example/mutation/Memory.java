package tech.habegger.graphql.example.mutation;

public class Memory<T> {
    T data;

    public void set(T newData) {
        this.data = newData;
    }

    public T get() {
        return data;
    }
}
